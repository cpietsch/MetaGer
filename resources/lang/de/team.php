<?php

return [
    'role.1'    => 'Geschäftsführender Vorstand/CEO',
    'role.2'    => 'Büroleiter und Jugendschutzbeauftragter',
    'role.3'    => 'Pressereferentin',
    'role.4'    => 'Programmierer',
    'role.5'    => 'Programmierer',
    'role.6'    => 'Programmierer',
    'role.7'    => 'Sekretariat',

    'contact.1' => 'Fragen/Probleme zu MetaGer etc. bitte immer an <a href="mailto:office@suma-ev.de">office@suma-ev.de</a> mailen, oder das <a href="/kontakt/">Kontaktformular mit Verschlüsselung</a> benutzten, oder am besten: ins <a href="http://forum.suma-ev.de/" target="_blank" rel="noopener">MetaGer-Forum</a> schreiben. Wir können bei der Vielzahl der Anfragen nicht immer zeitnah antworten - im <a href="http://forum.suma-ev.de/" target="_blank" rel="noopener">MetaGer-Forum</a> können Ihnen wahrscheinlich auch andere MetaGer-Nutzer weiterhelfen, und von den Fragen und Antworten profitieren ALLE.',
    'contact.2' => 'Und falls Sie jemals eine EMail mit "seltsamem" Inhalt bekommen sollten, auf der unsere Namen und/oder unsere EMail Adressen als Absender genannt sind, dann lesen Sie bitte hier weiter: <a href="https://metager.de/wsb/fakemail/">https://metager.de/wsb/fakemail/</a>',
    'contact.3' => 'Nur in begründeten Ausnahmefällen, wenn Sie bestimmte Personen direkt erreichen wollen, sollten Sie an diese mailen. Denn Team-Mitglieder können Urlaub haben, krank sein usw.',
];
