<?php

return [
    "nav1"      	=>	"Recherche",
    "nav4"	        =>	"Forum",
    "nav5"	        =>	"Contact",
    "nav6"      	=>	"Equipe",
    "nav9"	        =>	"Aide",
    "nav10"     	=>	"Widget",
    "nav15"	        =>	"Services",
    "nav16"         =>	"Soutenir",
    "nav18"	        =>	"Contact",
    "nav19"	        =>	"Langue",
    "nav20"	        =>	"Aide",
    "gutscheine.1"	=>	"Bons d´achat",
    "gutscheine.2"	=>	"Bon d´achat Congstar",
    "gutscheine.3"	=>	"Bon d´achat Check24\r\n",
    "gutscheine.4"	=>	"Bon d´achat Handyflash",
    "gutscheine.5"	=>	"Bon d´achat Groupon",
    "gutscheine.6"	=>	"Bon d´achat Medion",
    "gutscheine.7"	=>	"Bon d´achat Navabi",
    "gutscheine.8"	=>	"Bon d´achat Netcologne",
    "gutscheine.9"	=>	"Bon d´achat Teufel",
    "gutscheine.10"	=>	"Tous les bons d´achats",
    "sumaev.1"  	=>	"MetaGer est développé et opéré par",
    "sumaev.2"  	=>	"SUMA-EV - Verein für freien Wissenszugang."
];