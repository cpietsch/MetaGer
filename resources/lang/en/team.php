<?php

return [
    "role.1"	=>	"chief executive, chairman",
    "role.2"	=>	"office supervisor und youth protection commissioner",
    "role.3"	=>	"press secretary",
    "role.4"	=>	"programmer",
    "role.5"	=>	"programmer",
    "role.6"	=>	"programmer",
    "role.7"	=>	"secretary",
    "contact.1"	=>	"Please mail your questions/problems about MetaGer etc. to <a href=\"mailto:office@suma-ev.de\">office@suma-ev.de</a> or use the <a href=\"/en/kontakt/\">contact form with encryption</a>, or preferably make a post in the <a href=\"http://forum.suma-ev.de/\" target=\"_blank\" rel=\"noopener\">MetaGer-forum</a>. Due to the amount of requests, we can not always answer promptly - in the <a href=\"http://forum.suma-ev.de/\" target=\"_blank\" rel=\"noopener\">MetaGer-forum</a> there are probably other users which can help you aswell, and the questions and answers benefit everyone.",
    "contact.2"	=>	"And: If getting any emails from us with strange contents please read more about this: <a href=\"https://metager.de/wsb/fakemail/\">https://metager.de/wsb/fakemail/</a>",
    "contact.3"	=>	"Only in reasoned exceptions, if you want to rech someone directly, you should mail them. Because team members might be on vacation, sick etc."
];